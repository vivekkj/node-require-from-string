# Installation
> `npm install --save @types/require-from-string`

# Summary
This package contains type definitions for require-from-string (https://github.com/floatdrop/require-from-string).

# Details
Files were exported from https://www.github.com/DefinitelyTyped/DefinitelyTyped/tree/master/types/require-from-string

Additional Details
 * Last updated: Wed, 23 Aug 2017 17:50:59 GMT
 * Dependencies: none
 * Global values: none

# Credits
These definitions were written by Ika <https://github.com/ikatyang>.
